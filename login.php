
<html>
<head>

	<title>Login</title>
	<!-- <link rel="shortcut icon" type="Images/png" href="Image/logo.png"> -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="Css/css.css">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<style>
body {background-image: url("assets/images/greybg.jpg");
        background-size:100%;
        background-repeat:no-repeat;}

</style>
<body>

<div class="container" style="margin-left:130; margin-top:10px;">


	<div class="row">
	<div class="container" style="margin-left:270; margin-top:10px;">

<img src="assets/images/srclogo.png" alt="SRC">
</div>
	<div style="margin:-500px 0px 0px 250px; background-color:rgba(250,250,250,0.8);" class="col-md-6 login-left">

		<h2> Login Here </h2>
		<form action="loginvalidation.php" method="post">
		<div class="form-group">
			<label>Email</label>
			<input type="email" name="email" class="form-control" style="border: 1px solid #7d8282!important;" required>	
			</div>
			
			<div class="form-group">
			<label>Password</label>
			<input type="password" name="password" class="form-control" id="psw" style="border: 1px solid #7d8282!important;" required>	<span><i id="eye" class="input-icon material-icons" style="position:relative; float: right; top:-31px; left:-4px; display:none;cursor:pointer;" onclick="Toggle()">remove_red_eye</i><a href="forgetpassword.php" style="color:#0e5a9c;font-style:italic;">Forgot your password?</a></span>
			</div>
			<br>
			
			<button type="submit" name="loginbtn" class="btn btn-primary"> Login </button> &nbsp &nbsp &nbsp<a href="signup.php" style="color:#0e5a9c;font-style:italic;">Don't have an account?</a>
			
		
			</form>
			 <script>
            // Change the type of input to password or text
                function Toggle() {
                    var temp = document.getElementById("psw");
                    if (temp.type === "password") {
                        temp.type = "text";
                    }
                    else {
                        temp.type = "password";
                    }
                }
            </script>
			
		<script>
		var myInput = document.getElementById("psw");
		myInput.onkeyup = function() {
          if(myInput.value.length != 0) 
		  {
              document.getElementById("eye").style.display = "block";
            } else 
			{
              document.getElementById("eye").style.display = "none";
            }
          }
          </script>
			

				
			<br><br>
			
		
	</div>

	

	
	</div>
	
	</div>

</div>



</body>
</html>