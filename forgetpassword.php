<?php
$connect = mysqli_connect("localhost","root","");
mysqli_select_db($connect,"election");


?>
<html>
<head>
<a href="login.php"><img src="assets/images/back.png"  title="Back to front page" style="width:50px;height:50px;"></a>

	<title>Forget Password</title>
	<link rel="shortcut icon" type="Images/png" href="Image/logo.png">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="Css/css.css">
</head>
<style>
body {background-image: url("assets/images/greybg.jpg");
        background-size:100%;
        background-repeat:no-repeat;}

</style>
<body>
<div class="login-box">
	<div class="row" style="margin-right: 300px;margin-left: 100px; margin-top:140px;">

	<div class="col-md-6 login-left" style="background-color:rgba(250,250,250,0.8);  ;margin-left:300px;">
		<h4> Forget Password </h4>
		<br>
		<form method="post">
		<div class="form-group">
			<label>Please Insert Valid Email</label>
			<input type="email" name="email" class="form-control" style="border: 1px solid #7d8282!important;" required>	
			</div>

			<div class="form-group">
			<br>
			</div>
			<button type="submit" name="submit" class="btn btn-primary"> Send </button>
		</form>
	</div>




</body>
<?php
function random_pin()
{
	$character_set_array = array();
	$character_set_array[] = array('count' => 5, 'characters' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
	$character_set_array[] = array('count' => 3, 'characters' => '0123456789');
	$temp_array = array();
	foreach ($character_set_array as $character_set) 
	{
		for ($i = 0; $i < $character_set['count']; $i++) 
		{
			$temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
		}
}
	shuffle($temp_array);
	return implode('', $temp_array);
}

if(isset($_POST['submit'])){
	$email = mysqli_real_escape_string($connect,$_POST['email']);
	$currdate = date("Y-m-d H:i:s");  	
	$after_date = date("Y-m-d H:i:s",strtotime("+1 hours", strtotime($currdate)));
	$email_check = mysqli_query($connect,"SELECT * FROM voter WHERE voter_Email='".$email."'");
	$count = mysqli_num_rows($email_check);
	
	if($count !=0){
	
		$pin = random_pin();

		$result = mysqli_query($connect,"update voter set voter_Pincode = '$pin', securedate = '$after_date' where voter_Email = '$email'");
		$subject = "Forgot Password Security Pin";
		$message = "You can reset your password by using this security pin and click the link at below:\n\n http://localhost/voting-system/resetpassword.php \n\nSecurity Pin: ".$pin." \n\nThe security pin is just only valid within 1 hour.\n\nThank you.";
		$from = 'From: SRC Voting System <srcvoting@gmail.com>' . "\r\n";
		
		mail($email,$subject,$message,$from);	
		
		echo "<script>Swal.fire({
		type: 'success',
		title: 'Valid email ~ Please Check your email',
		showConfirmButton: false,
		timer: 1500
		})</script>";
		echo "<meta http-equiv='refresh' content='2;url=http://localhost/voting-system/resetpassword.php'>";

	}
	else
	{
		echo "<script>Swal.fire({
		type: 'error',
		title: 'This email does not exist!',
		showConfirmButton: false,
		timer: 1500
		})</script>";
		echo "<meta http-equiv='refresh' content='2;url=http://localhost/voting-system/forgetpassword.php'>";
	}
}
?>
</html>