<!DOCTYPE html>
<html lang="en">

  <?php include "html_head.php" ?>
    
    <body>
    
    <?php 
    if(isset($_SESSION["voter_ID"])){
        $userData = $_SESSION["voter_ID"];
        $voter_id = $userData["voter_ID"];

    }
    include "header.php";
        if(isset($_GET["eid"]))
		{
			$eid=$_GET["eid"];
			$result=mysqli_query($db,"SELECT * from election_details where EID='$eid'");
            $row=mysqli_fetch_assoc($result);
            $result2=mysqli_query($db,"SELECT * from selection where selection_EID='$eid'");
            $num=mysqli_num_rows($result2);
		}
    ?>

    

    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="assets/images/left-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5><?php echo $row['E_title'] ?></h5>
                    </div>
                    <div class="left-text">
                        <p>Status: <a href="#"><?php if($row['E_status']==1){echo "Completed";} else{echo "On Going";} ?></a><br><br>
                        Number of Voters Participated: <a href="#"><?php echo $num ?></a></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->


    


    <!-- ***** Features Small Start ***** -->
    <section class="section" id="services">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme">
                    <?php
                        
                        $result8=mysqli_query($db,"SELECT * from result where result_EID='$eid'");
                        $checkexist=mysqli_fetch_assoc($result8);
                        if($checkexist==NULL)
                        {
                            $result3=mysqli_query($db,"SELECT * from candidate where candidate_EID='$eid'");
                            while($candidate=mysqli_fetch_assoc($result3))
                            {
                                $result4=mysqli_query($db,"SELECT COUNT(candidate_ID) from selection where candidate_ID='".$candidate['candidate_ID']."'");
                                $count=mysqli_fetch_assoc($result4);
                                mysqli_query($db,"INSERT INTO result(result_EID,candidate_ID,votes) VALUES('$eid','".$candidate['candidate_ID']."','".$count['COUNT(candidate_ID)']."')");
                            }
                        }

                     
                        $result5=mysqli_query($db,"SELECT * from result where result_EID='$eid' ORDER BY votes DESC");
                        $winner=1;
                        while($votes=mysqli_fetch_assoc($result5))
                        {
                            $result6=mysqli_query($db,"SELECT * from candidate where candidate_ID='".$votes['candidate_ID']."'");
                            $candi=mysqli_fetch_assoc($result6);
                            if($winner==1)
                            {
                                ?>
                                    <div class="item service-item">
                                        <div class="icon">
                                            <i><img src="assets/images/winner.png" alt=""></i>
                                        </div>
                                        <h5 class="service-title"><?php echo $candi['candidate_Name'] ?></h5>
                                        <p><a href="#"><?php echo $votes['votes'] ?></a> votes</p>
                                    </div>
                                <?php
                                $winner++;
                            }
                            else
                            {
                                ?>
                                    <div class="item service-item">
                                        <div class="icon">
                                            <i><img src="assets/images/candidate.png" alt=""></i>
                                        </div>
                                        <h5 class="service-title"><?php echo $candi['candidate_Name'] ?></h5>
                                        <p><a href="#"><?php echo $votes['votes'] ?></a> votes</p>
                                    </div>
                                <?php
                            }

                        }

                    ?>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Small End ***** -->


    


   <?php include "footer.php" ?>
    
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

  </body>
</html>