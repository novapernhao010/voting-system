<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<?php include("html_head.php"); 
?>

<style>
#box1
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
	background-color:white;
	color:black;
}
#password0
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
	background-color:white;
	color:black;
}
#password1
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
	background-color:white;
	color:black;
}
#password2
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
	background-color:white;
	color:black;
}
#box2{
	height:150px;
	font-size:12pt;
	border: 2px double grey;
  	border-radius: 4px;
	color:black;
}
#box3
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
	background-color:white;
	color:black;
}
.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
input:focus {
  
  border:2px double blue;
}
select {
        width: 540px;
		height:80px;
		font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
    }
    select:focus {
        min-width: 150px;
        width: 540px;
    }    
</style>

<body>
	
	<!-- page -->
	<?php
		if(isset($_SESSION["voter_ID"])){
			$row = $_SESSION["voter_ID"];

			$username = $row["voter_Name"];
			$email = $row["voter_Email"];
			$phone = $row["voter_PhoneNo"];
			$address = $row["voter_Address"];
			$academic = $row["voter_AcademicYear"];
			$faculty = $row["voter_Faculty"];
			$gender = $row["voter_Gender"];
			$password = $row["voter_Password"];
		}
	?>

	<!-- //page -->

	<!-- contact -->
	<div class="contact py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<a href="profile.php"><img src="assets/images/back.png"  title="Back to front page" style="width:50px;height:50px;"></a>

			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>Edit </span><span></span><span style="font-style:italic;">Profile</span>
			</h3>
			<!-- form -->

			<form action="#" method="post">
				<div class="contact-grids1 w3agile-6">
					<div class="row">
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Name</label>
							<input type="text" class="form-control" id="box1" name="name"  placeholder="<?php echo $username; ?>" value="<?php echo $username; ?>" required>
						</div>
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">E-mail</label>
							<input type="text" class="form-control"  id="box1" name="email" style="background-color:#D3D3D3;" placeholder="<?php echo $row['voter_Email']; ?>"  value="<?php echo $email; ?>" disabled>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Phone</label>
						<input type="text" class="form-control"  id="box1" name="phone" minlength="10" maxlength="10" placeholder="<?php echo $row['voter_PhoneNo']; ?>" value="<?php echo $phone; ?>" pattern="[0-9]+" required>
					</div>
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Gender</label><br>
						<select id="gender" name="gender" style="text-transform:capitalize;" value="<?php echo $gender; ?>">
								<option value="male">Male</option>
								<option value="female">Female</option>
								<option value="other">Other</option>
							</select>
					</div>
					</div>
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Faculty</label>
						<select id="faculty" name="faculty" style="text-transform:capitalize;" value="<?php echo $faculty; ?>">
								<option value="FIST">FIST</option>
								<option value="FET">FET</option>
								<option value="FOB">FOB</option>
								<option value="FOL">FOL</option>
							</select>					</div>
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Academic Year</label>
						<select id="academic" name="academic" style="text-transform:capitalize;" value="<?php echo $academic; ?>">
								<option value="Foundation">Foundation</option>
								<option value="Diploma">Diploma</option>
								<option value="Degree">Degree</option>
							</select>	
					</div>
					</div>
					<div class="contact-me animated wow slideInUp form-group">
						<label class="col-form-label">Address</label>
						<input type="text" name="address" id="box2" class="form-control"  placeholder="<?php echo $row['voter_Address']; ?>"  value="<?php echo $address; ?>" required>
					</div>
					<div class="contact-form">
						<input type="submit" value="Update Profile" name="edit_profile" onclick = "return confirmation1();" style="background-color:#C0C0C0; color:black;">
					</div>
				</div>
			</form>
			<!-- password -->
			<form action="#" method="post">
			<div class="contact-grids1 w3agile-6">
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Old Password</label>
							<input type="password" class="form-control" id="password0"name="oldpassword" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
							<input type="checkbox" onclick="TogglePass0()">Show Password
						</div>
					</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Password</label>
							<input type="password" class="form-control" id="password1" name="password"   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
							<input type="checkbox" onclick="TogglePass1()">Show Password
						</div>
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Confirm Password</label>
							<input type="password" class="form-control"  id="password2" name="conpassword"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
							<input type="checkbox" onclick="TogglePass2()">Show Password
						</div>
					</div>
					<div class="contact-form">
						<input type="submit" class="button" value="Update Password" name="edit_password" onclick="return confirmation2();" style="background-color:#C0C0C0; color:black;">
					</div>
				</div>
			</div>	
			</form>
			<!-- //form -->
		</div>
	</div>



</body>
</html>
<!-- jquery -->
<script src="assets/js/jquery-2.2.3.min.js"></script>
<!-- //jquery -->
<script>
function TogglePass0() {
  var x = document.getElementById("password0");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function TogglePass1() {
  var y = document.getElementById("password1");
  if (y.type === "password") {
    y.type = "text";
  } else {
    y.type = "password";
  }
}
function TogglePass2() {
  var z = document.getElementById("password2");
  if (z.type === "password") {
    z.type = "text";
  } else {
    z.type = "password";
  }
}
</script>
<script>
function confirmation1(){
	var r;
	r = confirm("Do you want to edit your profile ?");
    return r;
}

function confirmation2(){
	var r;
	r = confirm("Do you want to edit your password ?");
    return r;
}
</script>
<?php
if (isset($_POST['edit_profile'])) {

$username = $_POST["name"];
$phone = $_POST["phone"];
$address = $_POST["address"];
$gender = $_POST["gender"];
$faculty = $_POST["faculty"];
$academic = $_POST["academic"];
	$update = mysqli_query($db,"UPDATE voter SET voter_Name='$username',voter_PhoneNo='$phone',voter_Address = '$address',voter_Faculty = '$faculty',voter_AcademicYear='$academic',voter_Gender ='$gender' WHERE voter_Email = '$email'");
	$sql = "SELECT * FROM voter WHERE voter_Email = '$email'";

	$result = mysqli_query($db,$sql);

		$row = mysqli_fetch_array($result);
			$_SESSION['voter_ID'] = $row ;
       ?>
		<script type="text/javascript">
		  swal({title: "Update Profile Successful!",
			  icon: "success",
			  button: "Back to Profile"}).then(function(){location.replace('profile.php');});
		
		</script>
	  <?php
}
?>

<?php
if (isset($_POST['edit_password'])) {
	$password = $_POST["password"];
	$conpassword = $_POST["conpassword"];
	$oldpass = $_POST["oldpassword"];

	
	if($oldpass != $row["voter_Password"]){
		?>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>		
				<script type="text/javascript">
					swal({
					title: "Wrong Old Password!",
					text:"Please Try Again!",
					icon:"error"
					});
				</script>	
			<?php
	}else if($password != $conpassword){
		?>
				<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>		
				<script type="text/javascript">
					swal({
					title: "Password Does Not Match	!",
					text:"Please Try Again!",
					icon:"error"
					});
				</script>
		<?php
	}else{
		$update = mysqli_query($db,"UPDATE voter SET voter_Password='$password' WHERE voter_Email = '$email'");
		$sql = "SELECT * FROM voter WHERE voter_Email = '$email'";
		$result = mysqli_query($db,$sql);
		if(mysqli_num_rows ($result)== 1){
			$row = mysqli_fetch_array($result);
			if($row['voter_ID'] !== 0){
			$_SESSION['voter_ID'] = $row ;
		
				?>
				<script type="text/javascript">
				  swal({title: "Update Password Successful!",
					  icon: "success",
					  button: "Back to Profile"}).then(function(){window.location.href ="profile.php";});
				</script>
			  <?php
			}
	}
}
}
?>