<?php
include("registration.php");
?>
<html>
<head>

	<title>Register</title>
	<link rel="shortcut icon" type="Images/png" href="Image/logo.png">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="Css/css.css">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<style>
body {background-image: url("assets/images/greybg.jpg");
        background-size:100%;
        background-repeat:no-repeat;}

</style>
<body>
<!-- <a href="login.php"><img src="Image/back.png"  title="Back to front page" style="width:50px;height:50px;"></a> -->
<div class="container" style=" margin-top:13px; margin-left:-220px;  margin-right:10px;">
	<div class="">
	<div class="row">
    <div class="container" style="width:10px;height:600px;">

    <img src="assets/images/srclogo.png" alt="SRC">
</div>
    <div class="col-md-6 login-left" style="background-color:rgba(250,250,250,0.8); margin-left:-630px; ">
		<h2> Register Here </h2>
        <form action="registration.php" method="post" >

        <div class="row">
        <div class="col">
            <div class="row" >
            <div class="col">
		    <div class="form-group">
			<label>Full Name</label>
            <input type="text" name="username" class="form-control" required>	
            </div></div>
			<div class="form-group">
			<label>Phone Number</label>
            <input type="text" name="phone" id="phonenum" class="form-control" pattern="[0-9]+" required>
            <script>document.getElementById("phonenum").maxLength = "11";</script>
            </div></div></div></div>

            <div class="row">
            <div class="col" >
            <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z0-9.-]+\.[a-z0-9.-]+\.[a-z]{2,}$" required>	

            </div></div></div>

            <label for="gender">Gender</label>

            <select name="gender" id="gender">
             <option value="Male">Male</option>
             <option value="Female">Female</option>
             </select>

             &nbsp &nbsp &nbsp &nbsp &nbsp
<label for="faculty">Faculty</label>

<select name="faculty" id="faculty">
  <option value="FIST">FIST</option>
  <option value="FOL">FOL</option>
  <option value="FOB">FOB</option>
  <option value="FET">FET</option>
</select>
&nbsp &nbsp &nbsp &nbsp 


<label for="academic">Academic Year</label>

<select name="academic" id="cars">
  <option value="Foundation">Foundation</option>
  <option value="Diploma">Diploma</option>
  <option value="Degree">Degree</option>

</select>
            <div class="row">
            <div class="col" >
            <div class="form-group">
            <label>Address</label>
            <textarea type="text" name="address" class="form-control" required></textarea>
            </div></div></div>

            <div class="row">
            <div class="col">
			<div class="form-group">
			<label>Password</label>
			<input type="password" name="password_1" class="form-control" id="psw1"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required><span><i id="eye1" class="input-icon material-icons" style="position:relative; float: right; top:-31px; left:-4px; display:none;cursor:pointer;" onclick="Toggle1()">remove_red_eye</i></span>
            </div></div>
            <div class="form-group">
			<label>Confirm Password</label>
			<input type="password" name="password_2" class="form-control" id="psw2"  required><span><i id="eye2" class="input-icon material-icons" style="position:relative; float: right; top:-31px; left:-4px; display:none;cursor:pointer;" onclick="Toggle2()">remove_red_eye</i></span>
			</div></div>

            <div class="row">
            <div class="col">
            <label class="checkbox-inline" >Verification Code</label>
            <img src="captcha.php" >
            <input type="text" name="vercode" class="form-control" placeholder="Verfication Code" required="required"></div>
            <a href="login.php" style="margin-top:34px;">&nbspAlready have an account? &nbsp</a>
            <div class="form-group small clearfix" style="margin-top:29px;">
               <button type="submit" class="btn btn-primary" name="signup"> Register </button></div>
            </div>

        
            
		</form>
	</div>
	
	</div>

    <script>
            // Change the type of input to password or text
                function Toggle1() {
                    var temp1 = document.getElementById("psw1");
                    if (temp1.type === "password") {
                        temp1.type = "text";
                    }
                    else {
                        temp1.type = "password";
                    }
                }
                function Toggle2() {
                    var temp2 = document.getElementById("psw2");
                    if (temp2.type === "password") {
                        temp2.type = "text";
                    }
                    else {
                        temp2.type = "password";
                    }
                }
            </script>
			
		<script>
		var myInput1 = document.getElementById("psw1");
		myInput1.onkeyup = function() {
          if(myInput1.value.length != 0) 
		  {
              document.getElementById("eye1").style.display = "block";
            } else 
			{
              document.getElementById("eye1").style.display = "none";
            }
          }
          var myInput2 = document.getElementById("psw2");
		myInput2.onkeyup = function() {
          if(myInput2.value.length != 0) 
		  {
              document.getElementById("eye2").style.display = "block";
            } else 
			{
              document.getElementById("eye2").style.display = "none";
            }
          }
          </script>
          <script>
            window.onload = function () {
            document.getElementById("psw1").onchange = validatePassword;
            document.getElementById("psw2").onchange = validatePassword;
        }
            function validatePassword() {
            var pass1 = document.getElementById("psw1").value;
            var pass2 = document.getElementById("psw2").value;
            if (pass1 != pass2)
                document.getElementById("psw2").setCustomValidity("Password do not match");
            else
                document.getElementById("psw2").setCustomValidity('');
            //empty string means no validation error
        }
              </script>

</div>




</body>
</html>
<?php

?>