<?php include "config.php";?>
<!DOCTYPE html>
<html lang="en">
<title>Vote History</title>
  <?php include "html_head.php" ?>
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <body>
    
    <style>
#vhistory {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#vhistory td, #vhistory th {
  border: 1px solid #ddd;
  padding: 8px;
}

.column1{
    width: 30%;
}

.column2{
    width: 20%;
}

.column3{
    width: 20%;
}
.column4{
    width: 1s0%;
}

#vhistory tr:nth-child(even){background-color: #f2f2f2;}

/*#vhistory tr:hover {background-color: #ddd;}*/

#vhistory th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #FF7800;
  color: white;
}
</style>
    <?php 
    if(isset($_SESSION["voter_ID"])){
        $userData = $_SESSION["voter_ID"];
        $voter_id = $userData["voter_ID"];

    }
    include "header.php";
    ?>


    <!-- ***** Welcome Area Start ***** -->
    <!--<div class="welcome-area" id="welcome">-->

        <!-- ***** Header Text Start ***** -->
        <!--<div class="header-text" style="background-color:white;">
            <div class="container">
                <div class="row">
                    <div class="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <h1><strong>President</strong> Election</h1>
                        <p>We provide e-voting service for MMU student to participate in SRC Election.</p>
                        <a href="#about" class="main-button-slider">Find Out More</a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <img src="assets/images/slider-icon.png" class="rounded img-fluid d-block mx-auto" alt="First Vector Graphic">
                    </div>
                </div>
            </div>
        </div>
        ***** Header Text End ***** -->
    <!--</div>-->
    <!-- ***** Welcome Area End ***** -->


    <!-- ***** Features Big Item Start ***** -->
    <!--<section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="assets/images/left-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5>Election candidates information provided is accurate</h5>
                    </div>
                    <div class="left-text">
                        <p>All students have their rights to vote. We will never influence yours decision and we defend student's fairness and justice. All the voting progress 
                            is onscreen and viewable to every student after the election ended.</p>
                        <a href="#about2" class="main-button">Discover More</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>-->
    <!-- ***** Features Big Item End ***** -->
 
    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12" style="text-align:center;">
                <h3>Vote History</h3>
                </div>
            </div>
            <div class="row" style="margin-top:20px;">
            <table id="vhistory">
                <tr>
                    <th class="column1">Election Title</th>
                    <th class="column2">Election Status</th>
                    <th class="column3">Voted For</th>
                    <th class="column4">Action</th>
                </tr>

            <?php
            $result = mysqli_query($db,"SELECT * FROM selection s INNER JOIN voter v ON s.voter_ID = v.voter_ID 
            INNER JOIN candidate c ON s.candidate_ID = c.candidate_ID INNER JOIN election_details ed ON s.selection_EID = ed.EID WHERE s.voter_ID = '$voter_id';");
            while($row = mysqli_fetch_assoc($result)){
                $eid = $row['selection_EID'];
                ?>
                <tr>
                    <td><?php echo $row['E_title'];?></td>
                    <td><?php if($row['E_status']==1){echo "<span style='color:green'>Completed</span>";}else{echo "<span style='color:red'>On-going</span>";}?></td>
                    <td><?php echo $row['candidate_Name'];?></td>
                    <td><?php if($row['E_status']==1){echo '<a href="result.php?eid='.$eid.'" class="w3-button w3-blue">View Result</a>';}else{echo "Election is not end yet.";}?></td>
                </tr>
                <?php
            }
                       
            ?>
                <!--<tr>
                    <td>Presidential Election 2020</td>
                    <td>On-going</td>
                    <td>Pern Juin Hao</td>
                    <td><span>Election is not end yet</span></td>
                </tr>
                <tr>
                    <td>General Election 2020</td>
                    <td style="color:green;">Completed</td>
                    <td>Teo Hong Xin</td>
                    <td><a href="result.php?eid=1" class="w3-button w3-blue">View Result</a></td>
                </tr>
                <tr>
                    <td>Head of Faculty Election 2021</td>
                    <td style="color:green;">Completed</td>
                    <td>Lee Hau Hwa</td>
                    <td><a href="" class="w3-button w3-blue">View Result</a></td>
                </tr>-->
                </table>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->

    <!-- ***** Features Big Item Start ***** -->
    <!--<section class="section" id="about2" style="background-color:#2596be;">
        <div class="container" >
            <div class="row">
                <div class="left-text col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix">
                    <div class="left-heading">
                        <h5>General Election</h5>
                    </div>
                    <ul>
                        <li>
                            <img src="assets/images/about-icon-01.png" alt="">
                            <div class="text">
                                <h6>Election Status</h6>
                                <p>Completed on 1/18/2021</p>
                            </div>
                        </li>
                        <li>
                            <img src="assets/images/about-icon-02.png" alt="">
                            <div class="text">
                                <h6>Participation Status</h6>
                                <p>Well done, you have done your vote! Thank you.</p>
                            </div>
                        </li>
                        <li>
                            <img src="assets/images/about-icon-03.png" alt="">
                            <div class="text">
                                <h6>Election Result</h6>
                                <p>A notification email will be sent when an election is ended.</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="right-image col-lg-7 col-md-12 col-sm-12 mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <img src="assets/images/right-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
            </div>
        </div>
    </section>-->
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Features Small Start ***** -->
    <!--<section class="section" id="services">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme">
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="assets/images/service-icon-01.png" alt=""></i>
                        </div>
                        <h5 class="service-title">Lee Hau Hwa</h5>
                        <p>Lee Hau Hwa is the right choice, go ahead, raise your voice!</p>
                        <a href="#" class="main-button">More Detail</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="assets/images/service-icon-02.png" alt=""></i>
                        </div>
                        <h5 class="service-title">Goh Ting Hong</h5>
                        <p>Pellentesque vitae urna ut nisi viverra tristique quis at dolor. In non sodales dolor, id egestas quam. Aliquam erat volutpat. </p>
                        <a href="#" class="main-button">More Detail</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="assets/images/service-icon-03.png" alt=""></i>
                        </div>
                        <h5 class="service-title">Pern Juin Hao</h5>
                        <p>Quisque finibus libero augue, in ultrices quam dictum id. Aliquam quis tellus sit amet urna tincidunt bibendum.</p>
                        <a href="#" class="main-button">More Detail</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="assets/images/service-icon-02.png" alt=""></i>
                        </div>
                        <h5 class="service-title">Fourth Service Box</h5>
                        <p>Fusce sollicitudin feugiat risus, tempus faucibus arcu blandit nec. Duis auctor dolor eu scelerisque vestibulum.</p>
                        <a href="#" class="main-button">Read More</a>
                    </div>
                   
                   
                    
                </div>
            </div>
        </div>
    </section>-->
    <!-- ***** Features Small End ***** -->


   


   <?php include "footer.php" ?>
    
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

  </body>
</html>