<!DOCTYPE html>
<html lang="en">
<title>Vote List</title>
  <?php include "html_head.php" ?>
    
    <body>
    
    <?php 
    include "config.php";
    if(isset($_SESSION["voter_ID"])){
        $userData = $_SESSION["voter_ID"];
        $voter_id = $userData["voter_ID"];

    }
    include "header.php";
    ?>

    <!-- ***** Welcome Area Start ***** -->
    <!--<div class="welcome-area" id="welcome">-->

        <!-- ***** Header Text Start ***** -->
        <!--<div class="header-text" style="background-color:white;">
            <div class="container">
                <div class="row">
                    <div class="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <h1><strong>President</strong> Election</h1>
                        <p>We provide e-voting service for MMU student to participate in SRC Election.</p>
                        <a href="#about" class="main-button-slider">Find Out More</a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <img src="assets/images/slider-icon.png" class="rounded img-fluid d-block mx-auto" alt="First Vector Graphic">
                    </div>
                </div>
            </div>
        </div>
        ***** Header Text End ***** -->
    <!--</div>-->
    <!-- ***** Welcome Area End ***** -->


    <!-- ***** Features Big Item Start ***** -->
    <!--<section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="assets/images/left-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5>Election candidates information provided is accurate</h5>
                    </div>
                    <div class="left-text">
                        <p>All students have their rights to vote. We will never influence yours decision and we defend student's fairness and justice. All the voting progress 
                            is onscreen and viewable to every student after the election ended.</p>
                        <a href="#about2" class="main-button">Discover More</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>-->
    <!-- ***** Features Big Item End ***** -->

    <section class="section" id="about2">
        <div class="container">
            <div class="row">
                <?php
                $result = mysqli_query($db,"SELECT * FROM election_details WHERE E_status = 0");
                while($election = mysqli_fetch_assoc($result)){
                    ?>
                    <div class="col-md-4">
                        <div class="item service-item" style="background-color:#FF7800;">
                            <h5 class="service-title" style="color:white;"><?php echo $election['E_title'];?></h5>
                            <p style="color:white;">End on 25 January 2021</p>
                            <a href="election.php?eid=<?php echo $election['EID'];?>" class="main-button">Vote</a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                
            </div>
        </div>
    </section>
    
    <!-- ***** Features Big Item Start ***** -->
    <!--<section class="section" id="about2">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="item service-item" style="background-color:#FF7800;">
                        <h5 class="service-title" style="color:white;">Presidential Election 2020</h5>
                        <p style="color:white;">End in 21days</p>
                        <a href="election.php?eid=1" class="main-button">Vote</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item service-item" style="background-color:#FF7800;">
                        <h5 class="service-title" style="color:white;">General Election 2021</h5>
                        <p style="color:white;">End in 11days</p>
                        <a href="#" class="main-button">Vote</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item service-item" style="background-color:#FF7800;">
                        <h5 class="service-title" style="color:white;">Head of Faculty Election 2020</h5>
                        <p style="color:white;">End in 3days</p>
                        <a href="#" class="main-button">Vote</a>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!-- ***** Features Big Item End ***** -->

    <!-- ***** Features Big Item Start ***** -->
    <!--<section class="section" id="about2" style="background-color:#2596be;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="item service-item">
                        <h5 class="service-title">Head of Faculty Election 2019</h5>
                        <p>End in 31days</p>
                        <a href="#" class="main-button">Vote</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item service-item">
                        <h5 class="service-title">Presidential Election 2021</h5>
                        <p>End in 2days</p>
                        <a href="#" class="main-button">Vote</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item service-item">
                        <h5 class="service-title">General Election 2019</h5>
                        <p>End in 20minutes</p>
                        <a href="#" class="main-button">Vote</a>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!-- ***** Features Big Item End ***** -->


   <?php include "footer.php" ?>
    
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

  </body>
</html>