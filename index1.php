<!DOCTYPE html>
<html lang="en">

  <?php include "html_head.php"?>
    
    <body>
    <?php
		if(isset($_SESSION["voter_ID"])){
            $userData = $_SESSION["voter_ID"];
            $voter_id = $userData["voter_ID"];

		}
	?>
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="#" class="logo">e<span style="color:#1B70E5;">MM</span><span style="color:#CD261C;">U</span></a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="index1.php" class="active">Home</a></li>
                            <li class="scroll-to-section"><a href="votelist.php">Election</a></li>
                            <li class="scroll-to-section"><a href="contact_us.php">Contact Us</a></li>
                            <?php
                            if(isset($_SESSION['voter_ID'])){
                                $result=mysqli_query($db,"SELECT * from voter where voter_ID='$voter_id'");
                                $voter=mysqli_fetch_assoc($result);
                                echo "<li class='submenu'>
                                        <a href='javascript:;'>".$voter['voter_Name']."</a>
                                        <ul>
                                            <li><a href='profile.php'>My Profile</a></li>
                                            <li><a href='votehistory.php'>My vote history</a></li>
                                            <li><a href='index1.php?logout='1''>Logout</a></li>
                                        </ul>
                                    </li>";}
                            else{
                                echo "<li class='scroll-to-section'><a href='login.php'>Log in</a></li>";}
                            
                            ?>
                            
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area" id="welcome">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <h1>SRC Election website is <strong>now ONLINE</strong></h1>
                        <p>We provide e-voting service for MMU student to participate in SRC Election.</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <img src="assets/images/slider-icon.png" class="rounded img-fluid d-block mx-auto" alt="First Vector Graphic">
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->


    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="assets/images/left-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5>Election candidates information provided is accurate</h5>
                    </div>
                    <div class="left-text">
                        <p>All students have their rights to vote. We will never influence yours decision and we defend student's fairness and justice. All the voting progress 
                            is onscreen and viewable to every student after the election ended.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about2">
        <div class="container">
            <div class="row">
                <div class="left-text col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix">
                    <div class="left-heading">
                        <h5>We provide students services</h5>
                    </div>
                    <p>To make your life easier, we have group together some important features.</p>
                    <ul>
                        <li>
                            <img src="assets/images/about-icon-01.png" alt="">
                            <div class="text">
                                <h6>Election History</h6>
                                <p>Student can view election history that held before.</p>
                            </div>
                        </li>
                        <li>
                            <img src="assets/images/about-icon-02.png" alt="">
                            <div class="text">
                                <h6>One student one vote</h6>
                                <p>As we mentioned before, fairness so each student only have one chance to cast their vote.</p>
                            </div>
                        </li>
                        <li>
                            <img src="assets/images/about-icon-03.png" alt="">
                            <div class="text">
                                <h6>Election Result</h6>
                                <p>A notification email will be sent when an election is ended.</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="right-image col-lg-7 col-md-12 col-sm-12 mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <img src="assets/images/right-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Features Small Start ***** -->
    <section class="section" id="services">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme">
                    <?php
                        $result2= mysqli_query($db,"SELECT * FROM election_details WHERE E_status = 0");
                        while($row2=mysqli_fetch_assoc($result2))
                        {
                            ?>
                                <div class="item service-item">
                                    <h5 class="service-title"><?php echo $row2['E_title'] ?></h5>
                                    <p></p>
                                    <a href="election.php?eid=<?php echo $row2['EID'] ?>" class="main-button">More Detail</a>
                                </div>
                            <?php

                        }

                    ?>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Small End ***** -->



   <?php include "footer.php" ?>
    
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

  </body>
</html>