<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">

<?php 
include("html_head.php"); 
?>
<style>
#box1
{
    height:80px;
	font-size:14pt;
	border: 2px solid grey;
  	border-radius: 4px;
}
#box2{
	height:150px;
	font-size:12pt;
	border: 2px double grey;
  	border-radius: 4px;
	color:black;
}
</style>

<body>
<?php
		if(isset($_SESSION["voter_ID"])){
			$row = $_SESSION["voter_ID"];

			$username = $row["voter_Name"];
			$email = $row["voter_Email"];
			$phone = $row["voter_PhoneNo"];
			$address = $row["voter_Address"];
			$faculty = $row["voter_Faculty"];
			$academic = $row["voter_AcademicYear"];
			$gender = $row["voter_Gender"];
			$password = $row["voter_Password"];
		}
	?>



	<!-- contact -->
	<div class="contact py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<a href="index1.php"><img src="assets/images/back.png"  title="Back to front page" style="width:50px;height:50px;"></a>
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>Profile</span>
			</h3>
			<!-- form -->

			<form action="#" method="post">
            <fieldset>
				<div class="contact-grids1 w3agile-6">
					<div class="row">
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">Name</label>
							<input type="text" class="form-control" id="box1" name="name"  value="<?php echo $row["voter_Name"] ?>" disabled>
						</div>
						<div class="col-md-6 col-sm-6 contact-form1 form-group">
							<label class="col-form-label">E-mail</label>
							<input type="text" class="form-control"  id="box1" name="email" value="<?php echo $row['voter_Email']; ?>"  disabled>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Phone</label>
						<input type="text" class="form-control"  id="box1" name="phone" value="<?php echo $row['voter_PhoneNo']; ?>"  disabled>
					</div>
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Gender</label>
						<input type="text" class="form-control"  id="box1" name="gender" style="text-transform:capitalize;" value="<?php echo $row['voter_Gender']; ?>"  disabled>
					</div>
					</div>
					<div class="row">
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Faculty</label>
						<input type="text" class="form-control"  id="box1" name="faculty" value="<?php echo $row['voter_Faculty']; ?>"  disabled>
					</div>
					<div class="col-md-6 col-sm-6 contact-form1 form-group">
						<label class="col-form-label">Academic Year</label>
						<input type="text" class="form-control"  id="box1" name="academic" style="text-transform:capitalize;" value="<?php echo $row['voter_AcademicYear']; ?>"  disabled>
					</div>
					</div>
					<div class="contact-me animated wow slideInUp form-group">
						<label class="col-form-label">Address</label>
						<input type="text" style="font-weight:bold;" name="address" id="box2" class="form-control" value="<?php echo $row['voter_Address']; ?>"  disabled> 
					</div>
					<div class="contact-form">
						<input type="submit" value="Edit Profile" formaction="editprofile.php" style="background-color:#C0C0C0; color:black;">
					</div>
				</div>
                </fieldset>
			</form>
			<!-- //form -->
		</div>
	</div>

</body>

</html>