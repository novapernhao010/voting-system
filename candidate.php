<!DOCTYPE html>
<html lang="en">

  <?php include "html_head.php" ?>
 
    
    <body>
    
    <?php 
    if(isset($_SESSION["voter_ID"])){
        $userData = $_SESSION["voter_ID"];
        $voter_id = $userData["voter_ID"];

    }
    include "header.php" ?>

    
    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
			<?php

				if(isset($_GET['can_id']))
				{
                    $can_id = $_GET["can_id"];
                    $eid = $_GET["eid"];
				    $sql = "SELECT * FROM candidate WHERE candidate_ID='$can_id'";
					$target_dir = "assets/images/candidate/";
					$result = mysqli_query($db,$sql);
					$row = mysqli_fetch_assoc($result);
				}
		?>
                <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="<?php echo $target_dir.$row['candidate_Image']; ?>" alt="<?php echo $row['candidate_Name'];?>" style="max-width: 100%;max-height: 100%;display:block;width: 200px;margin-left:auto;margin-right:auto;" class="rounded img-fluid d-block mx-auto" >
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5 style="font-size:50px"><?php echo $row["candidate_Name"];?></h5>
						
						<p><?php echo $row["candidate_Detail"];?></p>
                    </div>
                    <div class="left-text">
					<img src="assets/images/about-icon-01.png" alt="">
                      <span><?php echo $row["candidate_Gender"];?></span>
                    </div><br>
					 <div class="left-text">
					<img src="assets/images/about-icon-01.png" alt="">
                      <span><?php echo $row["candidate_Email"];?></span>
                    </div><br>
					 <div class="left-text">
					<img src="assets/images/about-icon-01.png" alt="">
                      <span><?php echo $row["candidate_Position"];?></span>
                    </div>
					<div class="left-heading">    
						<p style="border:3px dashed #66ccff; border-radius:10px; margin:20px; padding:20px; font-size:20px; color:#0099e6;"><?php echo $row["candidate_Slogan"];?></p>
                    </div><br><br><br><br>
					<div class="left-text">
                       <?php 
							if(isset($_SESSION["voter_ID"])){?>
							<div><a href="candidate.php?vote_id=<?php echo $row["candidate_ID"]?>&eid=<?php echo $eid?>" onclick="confirmation()" class="main-button">Vote Him</a></div>
						<?php
							}
							else {
						?>
							<div><a href="check.php" class="main-button">Vote Him</a></div>
						<?php
							}
						?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Features Small Start ***** -->
    <section class="section" id="services">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme">
				<?php
		
				    $sql = "SELECT * FROM candidate where candidate_EID='$eid'";
					$target_dir = "assets/images/candidate/";
					$result = mysqli_query($db,$sql);
					
					while ($row = mysqli_fetch_array($result))
					{

				?>
                    <div class="item service-item">
                        <div>
                            <i><img src="<?php echo $target_dir.$row['candidate_Image']; ?>" style="max-width: 100%;max-height: 100%;display:block;width: 200px;margin-left:auto;margin-right:auto;" alt="<?php echo $row['candidate_Name'];?>"></i>
                        </div><br>
                        <h5 class="service-title"><?php echo $row ["candidate_Name"];?></h5>
                        <p><?php echo $row ["candidate_Slogan"];?></p>
                        <a href="candidate.php?can_id=<?php echo $row["candidate_ID"]?>&eid=<?php echo $eid?>" class="main-button">More Detail</a>
                    </div>
				<?php
					}
				?>
                   
                    
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Small End ***** -->


   <?php include "footer.php" ?>
   <script type="text/javascript">
    function confirmation()
    {
        var result;
        result = confirm("Are you sure you want to vote him?");
        return result;
    }
    </script>
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

  </body>
</html>
<?php
#enter the selection
if(isset($_GET["vote_id"]))
{
    $candidate_id=$_GET["vote_id"];
    $eid = $_GET["eid"];

    $result=mysqli_query($db,"SELECT * from selection where selection_EID='$eid' and candidate_id='$candidate_id' and voter_ID='$voter_id'");
    if(mysqli_num_rows($result)>=1)
    {
        ?> 
				<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
				<script type="text/javascript">
				swal({title: "You have Voted Already!",
						icon: "error",
						button:false,
						timer:2000
					}).then(()=>{window.location.href = "election.php?eid="+<?php echo $eid ?>;});
					</script>
			<?php
            
    }
    else{
        mysqli_query($db,"INSERT INTO selection(voter_ID,candidate_ID,selection_EID) VALUES('$voter_id','$candidate_id','$eid')");
        header("Location:election.php?eid='$eid'");
        
    }

}
?>