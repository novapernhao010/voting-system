<!DOCTYPE html>
<html lang="en">

  <?php 
  include "html_head.php" ?>

 
    
    <body>
    
    <?php 
    if(isset($_SESSION["voter_ID"])){
        $userData = $_SESSION["voter_ID"];
        $voter_id = $userData["voter_ID"];

    }
    include "header.php" ?>


    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about2">
        <div class="container">
            <div class="row">
                <div class="left-text col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix"><br><br>
                    <div class="left-heading">
					<?php

							if(isset($_GET['eid']))
							{
								$eid = $_GET["eid"];
								$sql1 = "SELECT * FROM election_details WHERE EID='$eid'";
								$result1 = mysqli_query($db,$sql1);
								$row = mysqli_fetch_assoc($result1);
							}
					?>
                        <h5><?php echo $row["E_title"];?></h5>
                    </div>
                    <p><?php echo $row["E_date"];?><br> Polling Hours: FROM <?php echo $row["E_start"];?> TO <?php echo $row["E_end"];?></p>
                    <ul>
                        <li>
                            <img src="assets/images/about-icon-01.png" alt="">
                            <div class="text">
							
                                <h6>Status: <span style="color:#0099cc;"><?php if($row['E_status'] ==1){ echo " <span style='color:red;'>Ended Vote</span>";}else{ echo "Vote On Going";}?></span></h6>
								<br>
                            </div>
                        </li>
						<li>
                            <img src="assets/images/about-icon-01.png" alt="">
                            <div class="text">
                                <h6>Vote Position:
									<span style="color:#0099cc;">
										<?php 
											$result2 = mysqli_query($db,"SELECT * from candidate where candidate_EID='$eid'"); 
											$position=mysqli_fetch_assoc($result2); 
											echo $position["candidate_Position"]; 
										?>
									</span>
								</h6>
								<br>
                            </div>
                        </li>
                       <li>
                            <img src="assets/images/about-icon-01.png" alt="">
                            <div class="text">
                                <h6>Number of Voters Participated: 
									<span style="color:#0099cc;">
										<?php 
											$result3 = mysqli_query($db,"SELECT * from selection where selection_EID='$eid'"); 
											$num=mysqli_num_rows($result3); 
											echo $num; 
										?>
									</span>
								</h6>
								<br>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="right-image col-lg-7 col-md-12 col-sm-12 mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <img src="assets/images/vote.jpg" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Features Small Start ***** -->
    <section class="section" id="services">		
        <div class="container">
				<h2 style="color:white;">Candidates of Vote:</h2><br>
            <div class="row">
                <div class="owl-carousel owl-theme">
				
				<?php
		
				    $sql = "SELECT * FROM candidate where candidate_EID='$eid'";
					$target_dir = "assets/images/candidate/";
					$result = mysqli_query($db,$sql);
					
					while ($row = mysqli_fetch_assoc($result))
					{

				?>
                    <div class="item service-item">
                        <div>
                            <i><img src="<?php echo $target_dir.$row['candidate_Image']; ?>" style="width:300px; height:450px; max-height:100%; max-width:100%;" alt="<?php echo $row['candidate_Name'];?>"></i>
                        </div><br>
                        <h5 class="service-title"><?php echo $row ["candidate_Name"];?></h5>
                        <p><?php echo $row ["candidate_Slogan"];?></p>
                        <div><a href="candidate.php?can_id=<?php echo $row["candidate_ID"]?>&eid=<?php echo $eid?>" class="main-button">More Detail</a></div>
						<?php 
							if(isset($_SESSION["voter_ID"])){?>
							<div><a href="election.php?vote_id=<?php echo $row["candidate_ID"]?>&eid=<?php echo $eid?>" onclick="confirmation()" class="main-button">Vote Him</a></div>
						<?php
							}
							else {
						?>
							<div><a href="check.php" class="main-button">Vote Him</a></div>
						<?php
							}
						?>
						
                    </div>
				<?php
					}
				?>
                   
                    
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Small End ***** -->
	
	
	
<script type="text/javascript">
    function confirmation()
    {
        var result;
        result = confirm("Are you sure you want to vote him?");
        return result;
    }
</script>

   <?php include "footer.php" ?>
    
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

  </body>
</html>
<?php
#enter the selection
if(isset($_GET["vote_id"]))
{
    $candidate_id=$_GET["vote_id"];

    $result=mysqli_query($db,"SELECT * from selection where selection_EID='$eid' and candidate_id='$candidate_id' and voter_ID='$voter_id'");
    if(mysqli_num_rows($result)>=1)
    {
        ?> 
				<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
				<script type="text/javascript">
				swal({title: "You have Voted Already!",
						icon: "error",
						button:false,
						timer:2000
					}).then(()=>{window.location.href = "election.php?eid="+<?php echo $eid ?>;});
					</script>
			<?php
            
    }
    else{
        mysqli_query($db,"INSERT INTO selection(voter_ID,candidate_ID,selection_EID) VALUES('$voter_id','$candidate_id','$eid')");
        header("Location:election.php?eid='$eid'");
        
    }

}
?>