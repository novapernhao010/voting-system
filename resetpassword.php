<?php
session_start();
$db= mysqli_connect("localhost","root","","election");// fill out database name
if($db === false)
{
   die("ERROR: Could not connect.". mysqli_connect_error());
}
?>
<html>
<head>
<a href="forgetpassword.php"><img src="assets/images/back.png"  title="Back to front page" style="width:50px;height:50px;"></a>

	<title>Reset Password</title>
	<link rel="shortcut icon" type="Images/png" href="Image/logo.png">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="Css/css.css">
</head>
<style>
body {background-image: url("assets/images/greybg.jpg");
        background-size:100%;
        background-repeat:no-repeat;}

</style>
<body>
<div class="login-box">
	<div class="row"  >
	<div class="col-md-6 login-left" style="background-color:rgba(250,250,250,0.8); margin-top:110px; margin-left:330px;">
		<h4> Reset Password </h4>

		<form method="post">
		<div class="form-group">
			<br>
            <input  placeholder="Security Pin" type="text" name="pin" size="50" class="form-control" style="border: 1px solid #7d8282!important;" required autocomplete="off">
            <br>
            <input type="password" id="psw1" name="password" placeholder="New Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" class="form-control" style="border: 1px solid #7d8282!important;" required autofocus oninput="CheckPassword()" maxlength="30"><span><i id="eye1" class="input-icon material-icons" style="position:relative; float: right; top:-31px; left:-4px; display:none;cursor:pointer;" onclick="Toggle1()">remove_red_eye</i></span>				 
            <input type="password" id="psw2" name="re-password"  placeholder="Re-enter Password"  title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" class="form-control" style="border: 1px solid #7d8282!important;" autofocus maxlength="30" oninput="return Validatepass()">

        </div>

			<div class="form-group">
			<br>
			</div>
			<button type="submit" name="reset" class="btn btn-primary"> Save </button>
		</form>
	</div>


    <script>
            window.onload = function () {
            document.getElementById("psw1").onchange = validatePassword;
            document.getElementById("psw2").onchange = validatePassword;
        }
            function validatePassword() {
            var pass1 = document.getElementById("psw1").value;
            var pass2 = document.getElementById("psw2").value;
            if (pass1 != pass2)
                document.getElementById("psw2").setCustomValidity("Password do not match");
            else
                document.getElementById("psw2").setCustomValidity('');
            //empty string means no validation error
        }
              </script>

</body>
<?php
if(isset($_POST["reset"]))
{ 

    $pin = $_POST['pin'];
    $new = $_POST['password'];
    $conf = $_POST["re-password"];
    $currdate = date('H:i:s');

        $email_check = mysqli_query($db, "select * from voter where securedate >= '$currdate' and voter_Pincode = '$pin'");
        $count = mysqli_num_rows($email_check);
        $row = mysqli_fetch_assoc($email_check);
        
        if($count != 0)
        {
                if($new == $conf)
                    {
                        mysqli_query($db,"update voter set voter_Password = '$new' where voter_Pincode = '$pin'");
                    ?>
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script type="text/javascript">
                        swal({title:"<?php echo 'Password Change Successful!'?>",text:"Kindly login your account with new password.",icon:"success"}).then(function(){window.location.href="login.php";});
                    </script>
                    <?php
                   }
                   else
                   {
                       ?>
                <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>        
                <script type="text/javascript">
                    swal({
                           title: "You have entered wrong password !",
                           text:"Both password must be same !",
                           icon:"error"
                       });
                </script>
            <?php
                   }    
        }
        else
        {
             ?>
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>        
            <script type="text/javascript">
                swal({
                    title: "Failed!",
                    text:"Please keyin correct Security Pin.",
                    icon:"error"
                    });
            </script>
    <?php
        }   
    }
?>
</html>